#include "wifiSoftAP.h"
#include "wifi.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "http_server_app.h"
#include "output_iot.h"
#include "dht11.h"
#include "ledc_app.h"
#include "ws2812b.h"
#include "math.h"

static EventGroupHandle_t s_wifi_event_group;
static struct dht11_reading dht11_last_data, dht11_cur_data;

void switch_data_callback(char *data, int len)
{
    if(*data == '1')
    {
        output_io_set_level(GPIO_NUM_21, 1);
    }
    else if(*data == '0')
    {
        output_io_set_level(GPIO_NUM_21, 0);
    }
}
void slider_data_callback(char *data, int len)
{
    char number[100];
    memcpy(number, data, len+1);
    int duty = atoi(number);
    // printf("%s\n",data);
    ledcSetDuty(0,duty);

}
void dht11_data_callback(void)
{
    char resp[100];
    sprintf(resp, "{\"temperature\": \"%.1f\", \"humidity\":\"%.1f\"}",dht11_last_data.temperature, dht11_last_data.humidity);
    dht11_response(resp, strlen(resp));
}

int HexadecimalToDecimal(char *hex) {
    int p = 0; 
    int decimal = 0;
    int r, i;
 
    for(i = strlen(hex) - 1 ; i >= 0 ; --i)
    {
        if(hex[i]>='0'&&hex[i]<='9'){
            r = hex[i] - '0';
        }
        else{
            r = hex[i] - 'A' + 10;
        }
        
        decimal = decimal + r * pow(16 , p);
        ++p;
    }
    
    return decimal;
}
void rgb_data_callback(char *data, int len)
{
    char red[10] = "";
    char green[3] = "";
    char blue[3] = "";
    printf("RGB: %s\n", data);

    strncpy(red, data, 2);
    printf("RED: %d\n", HexadecimalToDecimal(red));

    strncpy(green, data + 2, 2);
    printf("GREEN: %d\n", HexadecimalToDecimal(green));

    strncpy(blue, data + 4, 2);
    printf("BLUE: %d\n", HexadecimalToDecimal(blue));

    for(int i = 0; i < 8; i++)
    {
        Ws2812b_Set_Color(i, reddec, gredec, bludec);
    }
    Ws2812b_Update_Color();
}

void wifi_data_callback(char *data, int len)
{
    char ssid[30] = "";
    char pass[30] = "";
    char *pt = strtok(data, "@");
    if(pt)
        strcpy(ssid, pt);
    pt = strtok(NULL, "@");
    if(pt)
        strcpy(pass, pt);
    printf("%s\n%s\n",ssid,pass);

    set_SSID(ssid);
    set_PASS(pass);
    wifi_mode_t temp;
    if(esp_wifi_get_mode(&temp) == ESP_OK)
    {
      if(temp == WIFI_MODE_STA)
      {
        stationStop();
        // wifi_init_softap();
        wifi_init_sta();
      }
      else if(temp == WIFI_MODE_AP)
      {
        wifiAPStop();
        wifi_init_sta();
      }
    }
    
}

void app_main(void)
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    http_set_callback_dht11(dht11_data_callback);
    http_set_callback_switch(switch_data_callback);
    http_set_callback_slider(slider_data_callback);
    http_set_callback_wifi(wifi_data_callback);
    http_set_callback_rgb(rgb_data_callback);

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_ap();
    esp_netif_create_default_wifi_sta();
    wifi_init_softap();

    start_webserver();
    output_io_create(GPIO_NUM_21);
    DHT11_init(GPIO_NUM_22);
    ledcInit();
    ledcAddPin(GPIO_NUM_2, 0);
    while(1)
    {
        dht11_cur_data = DHT11_read();
        if(dht11_cur_data.status == 0)
        {
            dht11_last_data = dht11_cur_data;
        }
        vTaskDelay(1000/portTICK_RATE_MS);
    }

}
