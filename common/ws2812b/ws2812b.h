#ifndef __WS2812B_H_
#define __WS2812B_H_
#include "esp_err.h"
#include "hal/gpio_types.h"

void WS2812_Update_Color(void);
void WS2812_Set_Color(int index, int r, int g, int b);
void ws2812_init(int tx_pin, int number_led);
#endif
